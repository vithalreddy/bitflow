package cmd

import (
	"fmt"
	"strings"

	"github.com/bloom42/bitflow/dl"
	"github.com/bloom42/rz-go/v2"
	"github.com/bloom42/rz-go/v2/log"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "bitflow",
	Short: "Bloom's download service",
	Long:  "Bloom's download service. Visit https://bloom.sh for more information",
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		log.SetLogger(log.With(
			rz.Formatter(rz.FormatterCLI()),
			rz.Level(rz.InfoLevel),
		))
		var err error
		var download dl.Download

		// basic detection if argument is a .torrent file
		if strings.HasSuffix(args[0], ".torrent") {
			download, err = dl.Torrent(args[0], "")
		} else {
			download, err = dl.Magnet(args[0], "")
		}

		if err != nil {
			log.Fatal(err.Error())
		}
		log.Info(fmt.Sprintf("%s successfully downloaded in %s", download.Name, download.Path))
	},
}

func Execute() error {
	return rootCmd.Execute()
}
