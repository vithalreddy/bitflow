module github.com/bloom42/bitflow

require (
	github.com/RoaringBitmap/roaring v0.4.16 // indirect
	github.com/anacrolix/dht v0.0.0-20181123025733-9b0a8e862ccc // indirect
	github.com/anacrolix/go-libutp v0.0.0-20180808010927-aebbeb60ea05 // indirect
	github.com/anacrolix/log v0.0.0-20180808012509-286fcf906b48 // indirect
	github.com/anacrolix/missinggo v0.0.0-20181123102432-08c0c0b796c8 // indirect
	github.com/anacrolix/mmsg v0.0.0-20180808012353-5adb2c1127c0 // indirect
	github.com/anacrolix/torrent v0.0.0-20181121062509-35642c832b53
	github.com/astrolib/godotenv v1.3.0
	github.com/aws/aws-sdk-go v1.16.8
	github.com/bloom42/denv-go v1.0.0
	github.com/bloom42/rz-go/v2 v2.4.1
	github.com/bloom42/uuid-go v0.0.0-20190217115539-dc11954538a8
	github.com/certifi/gocertifi v0.0.0-20180905225744-ee1a9a0726d2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/getsentry/raven-go v0.2.0
	github.com/google/btree v0.0.0-20180813153112-4030bb1f1f0c // indirect
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/huandu/xstrings v1.2.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20181108003508-044398e4856c // indirect
	github.com/spaolacci/murmur3 v0.0.0-20180118202830-f09979ecbc72 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	golang.org/x/net v0.0.0-20181114220301-adae6a3d119a // indirect
	golang.org/x/sys v0.0.0-20181122145206-62eef0e2fa9b // indirect
	golang.org/x/text v0.3.0 // indirect
	golang.org/x/time v0.0.0-20181108054448-85acf8d2951c // indirect
)
